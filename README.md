# DeversiFi Backend Development Challenge

 A pseudo-trading bot in NodeJs
 
 This is a pseudo-trading bot using real-time price signal from Bitfinex API, and technical indicators such as price 
 moving averages in order to enter and close positions.
 
 Assumptions: No trading fees, and orders are maker orders and they are filled instantly.
 
 
 #Thoughts on performance impriovement:
 
 Performance could be improved by
 
 —> Streaming exchange data (avoids waiting for responses)
 -> Optimizing any computations necessary for the bot to operate in order to minimise latency before placing orders: 
 parallelize, distribute between child processes running on different CPUs.

 
 #Run:
 `node index.js --asset='BTC' --samplingperiod 1000 --maperiod1 50 --maperiod2 200 --usdbal 500000 --assetbal 0 --risk 0.05 --stoploss 0.01`
 
 or
 
 'npm start'