const fetch = require('node-fetch') // Fetch library for Node


exports.getPrice = async function(asset){

  let ticker;
  if(asset === 'btc') ticker = 'tBTCUSD';
  else if(asset === 'eth') ticker = 'tETH';

  if(!ticker) return new Error('\nError: Invalid asset specified');

  const url = 'https://api-pub.bitfinex.com/v2/'

  const pathParams = 'ticker/' + ticker; // Change these based on relevant path params. /last for last candle
  const queryParams = ''; // Change these based on relevant query params

  try {
    const req = await fetch(`${url}/${pathParams}?${queryParams}`)
    const response = await req.json();
    return response[7]
  }
  catch (err) {
    console.log(err);
    return null;
  }

};
