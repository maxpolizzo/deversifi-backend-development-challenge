exports.movingAverage = function(data, period){

    if(period > data.length) {
      console.warn("\nNot enough data to compute MA with period " + period);
      return null;
    }

    let dataSample = data.slice(data.length - period);

    let movingAverageValue = 0;

    dataSample.forEach((value) => {
      movingAverageValue += value;
    });

    movingAverageValue /= period;

    return movingAverageValue;

};

exports.pnl = function(startAmt, endAmt){

  return (endAmt - startAmt)/startAmt;

};