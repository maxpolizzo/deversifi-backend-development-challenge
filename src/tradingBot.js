const bitfinexAPIService = require('./bitfinexAPIService');
const utils = require('./utils');

class TradingBot {

  constructor(asset, samplingPeriodMs, MAPeriods, initialAccountBalanceUSD, initialAccountBalanceAsset, riskPct, stopLossPct) {

    if(asset === "ETH" || asset === "BTC"
      && samplingPeriodMs > 0
      && MAPeriods[0] < MAPeriods[1]
      && initialAccountBalanceUSD >= 0
      && initialAccountBalanceAsset >= 0
      && riskPct > 0
      && stopLossPct > 0){

      this.asset = asset; // ASSET TO BE TRADED BY THE BOT
      this.samplingPeriodMs = samplingPeriodMs; // SAMPLING PERIOD OF THE PRICE SIGNAL
      this.priceHistory = [];
      this.MAStatus = null; // TRUE: MA50 > MA200, FALSE: MA200 > MA50
      this.MAPeriods = MAPeriods; // PERIODS OF MOVING AVERAGES TO USE AS INDICATORS
      this.initialAccountBalanceUSD = initialAccountBalanceUSD;
      this.initialAccountBalanceAsset = initialAccountBalanceAsset;
      this.currentAccountBalanceUSD = initialAccountBalanceUSD;
      this.currentAccountBalanceAsset = initialAccountBalanceAsset;
      this.riskPct = riskPct; // MAXIMUM RISK
      this.stopLossPct = stopLossPct; // STOP LOSS MARGIN
      this.openPosition = null;
      this.botReady = true;

      console.log("\nTrading Bot initialized successfully."
        + "\nAsset: " + asset
        + "\nSampling Period (s): " + Math.floor(samplingPeriodMs / 1000)
        + "\nIndicators: " + MAPeriods[0] * Math.floor(samplingPeriodMs / 1000) + "s moving average / " + MAPeriods[1] * Math.floor(samplingPeriodMs / 1000) + "s moving average"
        + "\nRisk: " + Math.floor(100 * riskPct) + "%"
        + "\nStop-Loss Margin: " + Math.floor(100 * stopLossPct) + "%"
        + "\nInitial balances: " + initialAccountBalanceUSD + " USD" + " / " + initialAccountBalanceAsset + " " + asset);


    } else {

      this.botReady = false;

      console.log("\nTrading Bot could not be initialized, please enter correct flag options: " +
        "\n --> asset must be either 'ETH' or 'BTC'" +
        "\n --> maperiod1 must be less than maperiod2" +
        "\n --> risk and stoploss must be positive"
      )
    }


  }

  run(){

    if(this.botReady){

      const samplingPeriodMs = this.samplingPeriodMs;

      setInterval(async () => {

        const lastPrice = await this.updateData();

        this.checkOpenPosition(lastPrice);

        const { MAStatusChange, MAStatus } = this.checkIndicators();

        if(MAStatusChange && MAStatus && !this.openPosition) this.buy(lastPrice);

        else if(MAStatusChange && !MAStatus && this.openPosition) this.sell(lastPrice);

      }, samplingPeriodMs);

    }


  }

  async updateData() {

    let lastPrice;

    lastPrice = await bitfinexAPIService.getPrice('btc');

    if(!lastPrice && this.priceHistory.length) lastPrice = this.priceHistory[this.priceHistory.length -1];

    if(lastPrice){

      if(this.priceHistory.length === 200) this.priceHistory.shift();

      this.priceHistory.push(lastPrice);

      console.log("\nDATA-UPDATE:\nASSET: " + this.asset + "\nUSD PRICE: " + lastPrice);

    } else {
      console.log("\nDATA-UPDATE FAILED...");
    }

    const pnl = Math.floor(100 * utils.pnl(this.initialAccountBalanceUSD, this.currentAccountBalanceUSD));

    console.log("\nUSD BALANCE: " + this.currentAccountBalanceUSD);
    console.log("\n" + this.asset + " BALANCE: " + this.currentAccountBalanceAsset);

    console.log("\nREALISED PNL (USD) so far: " + pnl + " %");

    return lastPrice;

  }

  checkOpenPosition(lastPrice){

    if(this.openPosition){
      const openPositionPNL = utils.pnl(this.openPosition, lastPrice);

      if(openPositionPNL < 0 && Math.abs(openPositionPNL) > this.stopLossPct) {
        this.sell(lastPrice);
        this.openPosition = null;
      }
    }
  }



  checkIndicators(){

    let MA0;
    let MA1;

    MA0 = utils.movingAverage(this.priceHistory, this.MAPeriods[0]);
    MA1 = utils.movingAverage(this.priceHistory, this.MAPeriods[1]);

    if(MA0 && MA1 && this.MAStatus === null) {// INITIALIZE MA STATUS IF NEEDED
      this.MAStatus = MA0 > MA1;
      console.log("\nMA status initialized")
    }

    let MAStatusChange = false;

    if (MA0 && MA1 && this.MAStatus && MA0 < MA1){
      console.log("\n--> BEARISH MA CROSSING DETECTED...");
      this.MAStatus = false;
      MAStatusChange = true;
    }

    if (MA0 && MA1 && !this.MAStatus && MA0 > MA1){
      console.log("\n--> BULLISH MA CROSSING DETECTED...");
      this.MAStatus = true;
      MAStatusChange = true;
    }

    console.log("\nMA50: " + MA0 + "\nMA200: " + MA1);

    return { MAStatusChange, MAStatus: this.MAStatus };

  }

  buy(price){

    const amtUSD = this.riskPct * this.currentAccountBalanceUSD;

    const amtAsset = amtUSD * price;

    if(amtUSD > 0){

      this.currentAccountBalanceUSD -= amtUSD;

      this.currentAccountBalanceAsset += amtAsset;

      this.openPosition = price;

      console.log("\nBUY " + amtAsset + " " + this.asset + "@ " + price + "BTC/USD");

    } else {
      console.log("\nInsufficient accoutn balance...")
    }

  }

  sell(price){

    const amtAsset = this.currentAccountBalanceAsset;

    this.currentAccountBalanceAsset -= amtAsset;

    this.currentAccountBalanceUSD += amtAsset * price;

    this.openPosition = null;

    console.log("\nSELL " + amtAsset + " " + this.asset + "@ " + price + "BTC/USD");

    const PNL = utils.pnl(this.openPosition, price);

    console.log("\nTRADE PNL: ");


  }


}

module.exports = TradingBot;


