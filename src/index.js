const flags = require('flags');
const tradingBot = require('./tradingBot');

flags.defineString('asset', 'BTC', 'Asset');
flags.defineInteger('samplingperiod', 1000, 'Sampling Period');
flags.defineNumber('maperiod1', 50, 'Moving Average Period 1');
flags.defineNumber('maperiod2', 200, 'Moving Average Period 2');
flags.defineStringList('usdbal', 1000, "Initial USD balance");
flags.defineMultiString('assetbal', 0, "Initial asset balance");
flags.defineStringList('risk', 0.05, "Risk management");
flags.defineStringList('stoploss', 0.01, "Stop Loss");
flags.parse();

const asset = flags.get('asset');
const samplingPeriodMs = parseFloat(flags.get('samplingperiod'));
const MAPeriods = [parseFloat(flags.get('maperiod1')), parseFloat(flags.get('maperiod2'))];
const initialAccountBalanceUSD = parseFloat(flags.get('usdbal')[0]);
const initialAccountBalanceAsset = parseFloat(flags.get('assetbal')[0]);
const riskPct = parseFloat(flags.get('risk')[0]);
const stopLossPct = parseFloat(flags.get('stoploss')[0]);

const TradingBot = new tradingBot(asset, samplingPeriodMs, MAPeriods, initialAccountBalanceUSD, initialAccountBalanceAsset, riskPct, stopLossPct);

TradingBot.run();
